package net.danafix.smsc.dinstar.service;

import net.danafix.smsc.dinstar.component.SmsHttpApi;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.stereotype.Component;

import java.lang.invoke.MethodHandles;
import java.util.List;
import java.util.Map;

//@Component
public class DlrChecker implements ApplicationRunner {
    @Autowired
    ThreadPoolTaskScheduler taskScheduler;
    @Autowired
    SmsHttpApi smsHttpApi;

    @Autowired
    NamedParameterJdbcTemplate jdt;
    private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    @Override
    public void run(ApplicationArguments args) throws Exception {
        taskScheduler.scheduleWithFixedDelay(new DlrCheckerWorker(), 900000);
    }

    private class DlrCheckerWorker implements Runnable {
        @Override
        public void run() {
            try {
                String sql = " SELECT * FROM gsm_sms_log WHERE send_time < SUBDATE(NOW(), INTERVAL 15 MINUTE) AND send_time >= SUBDATE(NOW(), INTERVAL 3 HOUR) " +
                        " AND dlr_status='SENT_OK' ORDER BY send_time ";
                MapSqlParameterSource sqlParams = new MapSqlParameterSource();

                List<Map<String,Object>> rows = jdt.queryForList(sql, sqlParams);
                for (Map<String,Object> row : rows) {
                    long smsId = (long) row.get("sms_id");
                    JSONObject jsonStatus = smsHttpApi.getRealTimeStatus(smsId, ""+row.get("msisdn"));
                    if (jsonStatus==null) continue;
                    String status = jsonStatus.getString("status");
                    log.info("SMS #"+smsId+": "+status);

                    sqlParams.addValue("status", status);
                    sqlParams.addValue("smsId", smsId);
                    sql = " UPDATE gsm_sms_log SET dlr_status=:status WHERE sms_id=:smsId ";
                    jdt.update(sql, sqlParams);
                }
            } catch (Exception e) {
                log.error("Error: "+e,e);
            }
        }
    }
}

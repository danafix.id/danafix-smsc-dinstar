package net.danafix.smsc.dinstar.controller;

import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;
import net.danafix.smsc.dinstar.component.SUID;
import net.danafix.smsc.dinstar.component.SmsHttpApi;
import net.danafix.smsc.dinstar.component.SmsUtils;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.IOUtils;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.joda.time.DateTime;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.lang.invoke.MethodHandles;
import java.nio.charset.StandardCharsets;
import java.util.Enumeration;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@RestController
public class RestApi {
    private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
    
    @Autowired
    SmsUtils smsUtils;
    @Autowired
    SmsHttpApi smsHttpApi;
    @Autowired
    NamedParameterJdbcTemplate jdt;

    private static final String authKey = "1ef1d1cb35fa396208905134ef1cb428";
    long uuid;

    @RequestMapping(value = { "/smsreguler.php","/test" })
    public String send(HttpServletRequest request) {
        try {
            String msisdn = request.getParameter("number").trim();
            String text = cleanText(""+request.getParameter("message").trim());
            text = request.getParameter("message").trim();

            msisdn = msisdn.replaceAll("[^0-9]","").replaceFirst("^0","62");

            return sendViaInfobip(msisdn, text, UUID.randomUUID().toString(),"DANAFIX.ID").toString();
        } catch (Exception e) {
            log.error("Error sending: "+e,e);
        }
        return "ERROR";
    }


    public String sendGsm(HttpServletRequest request) {
        uuid = SUID.id();
        long smsId = 0;
        JSONObject status = null;
        try {
            String msisdn = request.getParameter("number").trim();
            String text = cleanText(""+request.getParameter("message").trim());

            log.info("Cleaned text: "+text);

            //if (true) return text;

            PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();
            Phonenumber.PhoneNumber phoneNumber = phoneUtil.parse(msisdn, "ID");

            msisdn = phoneUtil.format(phoneNumber, PhoneNumberUtil.PhoneNumberFormat.E164);

            smsId = smsUtils.addLog(msisdn, text);
            log.info("Message stored with Id: "+smsId);

            if (smsUtils.checkIfRepeated(smsId, msisdn) && request.getParameter("skiprepeat")==null) {
                smsUtils.updateStatus(smsId, smsUtils.STATUS_SMS_REPEATED);
                log.error("SMS repeated!");
                return "501|Repeated";
            }

            status = smsHttpApi.send(smsId, msisdn, text,-1);
            log.info("Message #"+smsId+", to: "+msisdn+" is "+status.getString("status"));

            if (!status.getBoolean("success")) {
                smsUtils.updateStatus(smsId, smsUtils.STATUS_SMS_SENT_FAILED, status.getString("status"), status.getInt("port"), status.getString("time"),status.getInt("count"));
                log.error("Sending failed!");
                return "7|Failed";
            }

            int statusCode = smsUtils.getMapStatus(status.getString("status"));

            smsUtils.updateStatus(smsId, statusCode, status.getString("status"), status.getInt("port"), status.getString("time"),status.getInt("count"));
            return "0|"+smsId;
        } catch (Exception e) {
            smsUtils.updateStatus(smsId, smsUtils.STATUS_INTERNAL_SERVER_ERROR);
            log.error("Error: "+e,e);
            return "7|Error";
        }
    }

    @RequestMapping(value = { "/smsregulerreport.php" })
    public String reportInfobip(HttpServletRequest request) {

        //{"messages":[{"to":"6281299893636","status":{"groupId":1,"groupName":"PENDING","id":26,"name":"PENDING_ACCEPTED","description":"Message sent to next instance"},"messageId":"1576138023065817073"}]}
        try {
            log.info("Received status request: "+request.getParameter("id"));

            JSONObject jsonReq = new JSONObject(request.getParameter("id"));
            String msgId = jsonReq.getString("sentId");
        } catch (Exception e) {
            log.error("Error: "+e,e);
            return "FAILED";
        }
        return "SENT";
    }

    public String reportGsm(HttpServletRequest request) {
        try {
            Enumeration<String> params = request.getParameterNames();
            int id = Integer.parseInt(request.getParameter("id"));

            final MapSqlParameterSource sqlParams = new MapSqlParameterSource();
            sqlParams.addValue("id", id);

            final String sql = " SELECT dlr_status FROM gsm_sms_log WHERE sms_id=:id ";

            int retry = 0;
            String status = null;
            do {
                status = null;
                status = jdt.queryForObject(sql, sqlParams, String.class);
                if (status == null) {
                    retry++;
                    Thread.sleep(2000);
                }
            } while (retry < 3 && status == null);
            if (status == null) {
                throw new Exception("Status is error!");
            }
            return status;
        } catch (org.springframework.dao.EmptyResultDataAccessException e) {
            return "ID NOT FOUND";
        } catch (Exception e) {
            log.error("Error getting status: "+e);
            return "FAILED";
        }
    }

    private String cleanText(String text0) {
        String text = text0.replaceAll("kode", "")
                .replaceAll("otp", "")
                .replaceAll("[ ]{2,10}"," ")
                .replaceAll("  ", " ")
                .replaceAll("   ", " ");

        Pattern p = Pattern.compile("-?\\d+");
        Matcher m = p.matcher(text);
        String otp = null;
        while (m.find()) {
            otp = m.group();
            break;
        }

        if (otp != null) {
            String newOtp = "";
            for (int i=0;i<otp.length();i++) {
                String digit = otp.substring(i,i+1);
                if (digit!=null)
                    newOtp += " "+otp.substring(i,i+1);
            }
            log.info("New OTP: "+newOtp);
            text = "Pengiriman ulang, masukkan "+newOtp+" \n\nDANAFIX.ID";
        }

        return text;
    }

    public JSONObject sendViaInfobip(String destMobile, String message, String refId, String senderId) throws Exception {
        boolean success = false;

        try {
            String mobile = destMobile.trim().replaceFirst("^0","62").replaceFirst("\\+","");
            JSONObject jsonReq = new JSONObject()
                    .put("to", mobile)
                    .put("from", senderId)
                    .put("text", message);

            String url = "https://api-id1.infobip.com/sms/2/text/single";
            JSONObject jsonResp = post(url, jsonReq);

            JSONObject respMsg = jsonResp.getJSONArray("messages").getJSONObject(0);
            JSONObject status = respMsg.getJSONObject("status");

            success = (status.getString("name").equals("PENDING_ACCEPTED"))?true:false;

            return new JSONObject()
                    .put("to", destMobile)
                    .put("sentId", respMsg.get("messageId"))
                    .put("sentOn", DateTime.now().toString("yyyy-MM-dd HH:mm:ss"))
                    .put("statusId", status.get("id"))
                    .put("statusName", status.get("name"))
                    .put("success", success);
        } catch (Exception e) {
            log.error("Error: "+e,e);
            return new JSONObject()
                    .put("to", destMobile)
                    .put("sentId", UUID.randomUUID().toString())
                    .put("sentOn", DateTime.now().toString("yyyy-MM-dd HH:mm:ss"))
                    .put("statusId", 999)
                    .put("statusName", ""+e)
                    .put("success", false);
        }
    }

    private final static RequestConfig defaultRequestConfig = RequestConfig.custom()
            .setSocketTimeout(30000)
            .setConnectTimeout(30000)
            .setConnectionRequestTimeout(30000)
            .build();

    private CloseableHttpClient createHttpClient() {
        HttpClientBuilder builder = HttpClients.custom()
                .setSSLHostnameVerifier(new NoopHostnameVerifier())
                .setDefaultRequestConfig(defaultRequestConfig);

        CloseableHttpClient httpClient = builder.build();
        return httpClient;
    }

    public JSONObject post(String url, JSONObject jsonReq) throws Exception {
        log.debug("Post to URL: "+url);
        CloseableHttpClient client = createHttpClient();

        HttpPost httpPost = new HttpPost(url);
        String body = jsonReq.toString();

        log.info("Http payload: "+body);

        String authorization = Base64.encodeBase64String("danafix.otp:D@naf1x!".getBytes());

        StringEntity entity = new StringEntity(body.toString());
        httpPost.setHeader("Content-type", "application/json");
        httpPost.setHeader("Authorization", "Basic "+authorization);
        httpPost.setEntity(entity);

        CloseableHttpResponse response = client.execute(httpPost);
        String respText = IOUtils.toString(response.getEntity().getContent(), StandardCharsets.UTF_8);

        log.info("Response http: "+respText);

        return new JSONObject(respText);
    }

}

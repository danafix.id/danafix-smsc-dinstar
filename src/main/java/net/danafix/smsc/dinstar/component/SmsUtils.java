package net.danafix.smsc.dinstar.component;

import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.PhoneNumberUtil.PhoneNumberFormat;
import com.google.i18n.phonenumbers.Phonenumber.PhoneNumber;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.lang.invoke.MethodHandles;
import java.sql.Types;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class SmsUtils {

	@Autowired
	NamedParameterJdbcTemplate jdt;

	private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
	private static HashMap<Integer, String> errorMessages = null;
	
	public static final int STATUS_OK = 200;
	public static final int STATUS_MESSAGE_SENT = 201;
	public static final int STATUS_MESSAGE_DELIVERED = 200;
	public static final int STATUS_INTERNAL_SERVER_ERROR = 500;	
	public static final int STATUS_INVALID_MOBILE = 401;
	public static final int STATUS_INVALID_REQUEST = 400;
	public static final int STATUS_SMS_REPEATED = 402;
	public static final int STATUS_FORBIDDEN = 403;
	public static final int STATUS_NOT_FOUND = 404;
	public static final int STATUS_QUEUE_EXCEEDED = 429;
	public static final int STATUS_SMS_SENT_FAILED = 501;
	public static final int STATUS_UNKNOWN = 600;
	
	public static final String GW_STATUS_SENT = "SENT_OK";
	public static final String GW_STATUS_DELIVERED = "DELIVERED";
	public static final String GW_STATUS_FAILED = "FAILED";
	
	
	public long addLog(String msisdn, String text) throws Exception {
		
		long smsId = 0;
		
		if (text == null) {
			throw (new Exception("No message parameter supplied!"));
		}		

		if (msisdn == null) {
			throw (new Exception("No msisdn parameter supplied!"));
		}		
		
		msisdn = msisdn.trim();
		text = text.trim();
		
		PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();
		PhoneNumber phoneNumber = phoneUtil.parse(msisdn, "ID");													
		
		msisdn = phoneUtil.format(phoneNumber, PhoneNumberFormat.E164);
		
		KeyHolder keyHolder = new GeneratedKeyHolder();
		String sql = " INSERT INTO gsm_sms_log (msisdn,message,status_code) VALUES (:msisdn,:message,991) ";
		HashMap<String, Object> params = new HashMap<String, Object>();
		params.put("msisdn", msisdn.trim());
		params.put("message", text.trim());
		SqlParameterSource paramSources = new MapSqlParameterSource(params);
		int affRow = jdt.update(sql, paramSources, keyHolder);
		
		smsId = keyHolder.getKey().longValue();
		
		return smsId;
		
	}
	
	public JSONObject createJsonError(HttpServletResponse response, int errorCode, boolean success) {
		response.setStatus(errorCode);
		if (errorMessages == null) {
			errorMessages = new HashMap<>();
			errorMessages.put(STATUS_FORBIDDEN, "Authorization required");
			errorMessages.put(STATUS_INTERNAL_SERVER_ERROR, "Internal server error");
			errorMessages.put(STATUS_INVALID_REQUEST, "Invalid request/incomplete parameter");
			errorMessages.put(STATUS_MESSAGE_SENT, "Message sent");
			errorMessages.put(STATUS_OK, "Request successfull");	
			errorMessages.put(STATUS_QUEUE_EXCEEDED, "Maximum queue exceeded");
			errorMessages.put(STATUS_SMS_SENT_FAILED, "SMS sending failed!");
			errorMessages.put(STATUS_SMS_REPEATED, "Similar SMS repeated!");
			errorMessages.put(STATUS_NOT_FOUND, "SMS status not found!");
		}
		
		JSONObject jsonResult = new JSONObject();
		jsonResult.put("success", success);
		jsonResult.put("statusCode", errorCode);
		jsonResult.put("statusMessage", errorMessages.get(errorCode));
		return jsonResult;
	}

	
	public void updateStatus(long smsId, int statusId, String dlrStatus, Integer port, String statusTime, Integer smsCount) {
		NamedParameterJdbcTemplate jdtMy = jdt;
		
		String sql = " UPDATE gsm_sms_log set status_code=:statusId, send_time=NOW(), dlr_status=:dlrStatus, gsm_port=:port, status_time=:statusTime, sms_count=:smsCount WHERE sms_id=:smsId ";
		
		MapSqlParameterSource paramMap = new MapSqlParameterSource();
		paramMap.addValue("smsId", smsId);
		
		paramMap.addValue("statusId", statusId);
		if (dlrStatus == null) {
			paramMap.addValue("dlrStatus", null, Types.NULL);
			paramMap.addValue("port", null, Types.NULL);
			paramMap.addValue("statusTime", null, Types.NULL);
			paramMap.addValue("smsCount", null, Types.NULL);
		}
		else {
			paramMap.addValue("dlrStatus", dlrStatus);
			paramMap.addValue("port", port);
			paramMap.addValue("statusTime", statusTime);
			paramMap.addValue("smsCount", smsCount);
		}
		
		jdtMy.update(sql, paramMap);		
	}
	
	public void updateStatus(long smsId, int statusId) {
		updateStatus(smsId, statusId, null, null, null, null);		
	}
	
	public boolean checkIfRepeated(long smsId, String msisdn) {
		String sql = " SELECT count(*) as ct from gsm_sms_log WHERE msisdn=:msisdn AND submit_time>=SUBDATE(NOW(), INTERVAL 15 SECOND) ";
		sql+= " AND sms_id!=:smsId AND status_code!=:errorCode ";
		MapSqlParameterSource paramMap = new MapSqlParameterSource();
		paramMap.addValue("msisdn", msisdn);
		paramMap.addValue("smsId", smsId);
		paramMap.addValue("errorCode", STATUS_SMS_REPEATED);
		int count = jdt.queryForObject(sql, paramMap, Integer.class);
		if (count > 0) return true;
		else return false;
	}
	 
	public String reformatMsisdn(String msisdn) throws Exception {
		PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();
		PhoneNumber phoneNumber = phoneUtil.parse(msisdn, "ID");															
		msisdn = phoneUtil.format(phoneNumber, PhoneNumberFormat.E164);
		return msisdn;
	}
	
	public Map<String,Object> getSms(long smsId) throws Exception {
		MapSqlParameterSource paramMap = new MapSqlParameterSource();
		paramMap.addValue("smsId", smsId);
		
		String sql = " SELECT * FROM gsm_sms_log WHERE sms_id=:smsId ";		
		List<Map<String,Object>> queryResult = jdt.queryForList(sql, paramMap);
		for (Map<String,Object> record : queryResult) {
			return record;
		}		
		return null;
	}
	
	
	public List<Map<String,Object>> getLocalUnreadInbox() {
		MapSqlParameterSource paramMap = new MapSqlParameterSource();
		paramMap.addValue("isRead", 0);
		
		String sql = " SELECT sms_id as id,sms_time as `datetime`,message as `text`,msisdn ";
		sql+= " FROM gsm_sms_inbound WHERE is_read=:isRead AND ins_on>=SUBDATE(NOW(), INTERVAL 1 DAY) ORDER BY sms_time LIMIT 0,500 ";
		List<Map<String,Object>> list = jdt.queryForList(sql, paramMap);
		for (Map<String,Object> record : list) {
			log.info("Updating as read sms id: "+record.get("id"));
			paramMap.addValue("smsId", record.get("id"));
			sql = " UPDATE gsm_sms_inbound SET is_read=1 WHERE sms_id=:smsId ";
			jdt.update(sql, paramMap);
		}		
		return list;
	}
	
	public List<Map<String,Object>> queryInbox(HttpServletRequest request) throws Exception {
		MapSqlParameterSource paramMap = new MapSqlParameterSource();
		paramMap.addValue("isRead", 0);
		
		String sql = " SELECT sms_id as id,sms_time as `datetime`,message as `text`,msisdn ";
		sql+= " FROM gsm_sms_inbound WHERE 1 AND ins_on>=SUBDATE(NOW(), INTERVAL 1 DAY) ";
		if (request.getParameter("msisdn")!=null) {
			String msisdn = request.getParameter("msisdn").trim();
			msisdn = msisdn.trim();			
			
			PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();
			PhoneNumber phoneNumber = phoneUtil.parse(msisdn, "ID");													
			
			msisdn = phoneUtil.format(phoneNumber, PhoneNumberFormat.E164);
			
			paramMap.addValue("msisdn", msisdn);
			sql+= " AND msisdn=:msisdn ";
		}
		
		int limit = (request.getParameter("limit")==null)?50:Integer.parseInt(request.getParameter("limit"));
		paramMap.addValue("limit", limit);
		sql+= " ORDER BY sms_time LIMIT 0,:limit ";
		List<Map<String,Object>> list = jdt.queryForList(sql, paramMap);	
		return list;
	}
	
	public List<Map<String,Object>> getLocalTodayInbox() {
		MapSqlParameterSource paramMap = new MapSqlParameterSource();
		paramMap.addValue("isRead", 0);
		
		String sql = " SELECT sms_id as id,sms_time as `datetime`,message as `text`,msisdn ";
		sql+= " FROM gsm_sms_inbound WHERE 1 AND ins_on>=CURRENT_DATE() ORDER BY sms_time LIMIT 0,50 ";
		List<Map<String,Object>> list = jdt.queryForList(sql, paramMap);
			
		return list;
	}
	
	public int getMapStatus(String dlrStatus) {
		int statusCode = SmsUtils.STATUS_UNKNOWN;
		
		if (dlrStatus.equals("DELIVERED")) statusCode = SmsUtils.STATUS_MESSAGE_DELIVERED;
		else if (dlrStatus.equals("SENT_OK")) statusCode = SmsUtils.STATUS_MESSAGE_SENT;
		else if (dlrStatus.equals("FAILED")) statusCode = SmsUtils.STATUS_SMS_SENT_FAILED;
		
		return statusCode;
	}
}

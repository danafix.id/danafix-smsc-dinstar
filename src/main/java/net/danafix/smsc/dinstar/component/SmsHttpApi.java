package net.danafix.smsc.dinstar.component;

import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.PhoneNumberUtil.PhoneNumberFormat;
import com.google.i18n.phonenumbers.Phonenumber.PhoneNumber;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import org.apache.http.client.utils.URIBuilder;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.lang.invoke.MethodHandles;
import java.util.HashMap;

@Component
public class SmsHttpApi {

	@Value("${gsm.host}")
	String gsmHost;
	@Value("${gsm.port}")
	int gsmPort;
	@Value("${gsm.username}")
	String gsmUser;
	@Value("${gsm.password}")
	String gsmPass;
	@Value("${gsm.ports}")
	String gsmPorts;
	
	private static HashMap<Integer, String> errorMessages = null;
	private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
	
	private URIBuilder getUrlBuilder(String path) {
		URIBuilder builder = new URIBuilder();
		builder.setHost(gsmHost);
		builder.setScheme("http");
		builder.setPort(gsmPort);
		builder.setPath(path);
		return builder;
	}
	
	private String getUrl(String path) throws Exception {
		
		URIBuilder builder = new URIBuilder();
		builder.setHost(gsmHost);
		builder.setScheme("http");
		builder.setPort(gsmPort);
		builder.setPath(path);
		return builder.build().toString();
	}
	
	public JSONObject send(long smsId, String msisdn, String text, int portIndex) throws Exception {
		PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();
		PhoneNumber phoneNumber = phoneUtil.parse(msisdn, "ID");															
		msisdn = phoneUtil.format(phoneNumber, PhoneNumberFormat.E164);
		
		String url = getUrl("/api/send_sms");						
		log.info("#"+smsId+" sending to : "+msisdn+", text: "+text+", URL: "+url);
		
		String[] gsmPortsString = gsmPorts.split(",");

		if (portIndex >=0) {
			String portStr = gsmPortsString[portIndex];
			gsmPortsString = new String[] { portStr };
		}
		
		int[] ports = new int[gsmPortsString.length];				
		for (int i = 0; i < gsmPortsString.length; i++) {
			ports[i] = Integer.parseInt(gsmPortsString[i]);
		}
		
		JSONArray bodyParam = new JSONArray();
		bodyParam.put((new JSONObject()).put("number", msisdn).put("user_id", smsId));
		
		JSONObject body = new JSONObject();
		body.put("text", text).put("param", bodyParam).put("port", ports);				
		
		HttpResponse<String> response = requestPost(url,body);
		log.info("#"+smsId+" Status: "+response.getStatus()+", response: "+response.getBody());
						
		int recheckCount = 0;
		String status = "SENDING";
		
		JSONObject jsonResult = null;
		do {			
			if (recheckCount > 5) throw new Exception("Sending timed out!");
			jsonResult = getRealTimeStatus(smsId, msisdn);
			
			if (jsonResult!=null)
				status = jsonResult.getString("status");		
			
			if (jsonResult==null || status.equals("SENDING")) {
				log.info("#"+smsId+" waiting for sending ....");
				Thread.sleep(4000);							
			}			
			recheckCount++;
		} while (jsonResult==null || status.equals("SENDING"));
						
		
		if (status.equals(SmsUtils.GW_STATUS_SENT) || status.equals(SmsUtils.GW_STATUS_DELIVERED)) {
			jsonResult.put("success", true);
		} else {
			jsonResult.put("success", false);
		}				
				
		return jsonResult;
	}
	
	private JSONObject getRealTimeStatus0(long smsId, String msisdn) throws Exception {
		String url = getUrl("/api/query_sms_result");
		JSONObject body = new JSONObject();
		body.put("number", (new JSONArray()).put(msisdn));
		body.put("user_id", (new JSONArray()).put(smsId));
		HttpResponse<String> response = requestPost(url,body);
		log.info("#"+smsId+" Status: "+response.getStatus()+", response: "+response.getBody().trim());		
		
		JSONObject jsonResult = new JSONObject(response.getBody());
		return jsonResult;		
	}
	
	public JSONObject getRealTimeStatus(long smsId, String msisdn) throws Exception {
		JSONObject jsonResult = getRealTimeStatus0(smsId, msisdn);
		JSONArray arrayResult = jsonResult.getJSONArray("result");
		if (arrayResult.length()>0) {
			arrayResult.getJSONObject(0).put("error_code", jsonResult.getInt("error_code"));
			return arrayResult.getJSONObject(0);
		}
									
		else return null;					
	}
	
	private HttpResponse<String> requestPost(String url, JSONObject body) throws UnirestException {
		
		HttpResponse<String> response = Unirest
				.post(url)
				.header("Content-type", "application/json")
				.basicAuth(gsmUser, gsmPass)
				.body(body)							
				.asString();		
		return response;
	}
	
	private HttpResponse<String> requestGet(String url) throws UnirestException {
		
		HttpResponse<String> response = Unirest
				.get(url)
				.header("Content-type", "application/json")
				.basicAuth(gsmUser, gsmPass)						
				.asString();		
		return response;
		
	}
	
	public JSONArray getReceivedMessage(String flag) throws Exception {
		URIBuilder builder = getUrlBuilder("/api/query_incoming_sms");
		builder.addParameter("flag", flag);
		String url = builder.build().toString();
		
		HttpResponse<String> response = requestGet(url);
		JSONObject jsonResponse = new JSONObject(response.getBody());
		return jsonResponse.getJSONArray("sms");
	}
	
}



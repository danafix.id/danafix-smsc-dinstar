package net.danafix.smsc.dinstar.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;

import javax.sql.DataSource;

@Configuration
public class Db {
    @Bean(name="ds1")
    @ConfigurationProperties(prefix="db1.datasource")
    public DataSource getDs1() {
        return DataSourceBuilder.create().build();
    }

    @Bean(name="jdt")
    public NamedParameterJdbcTemplate getJdbc1() {
        return new NamedParameterJdbcTemplate(getDs1());
    }
}
